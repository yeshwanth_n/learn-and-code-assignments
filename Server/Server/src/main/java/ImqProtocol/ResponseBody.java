package ImqProtocol;

import java.util.ArrayList;

public class ResponseBody {
	
    String responseStatus;
	String message;
	String messageStatus;
	private ArrayList topics;

	public String getMessage() {
		return message;
	} 

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getMessageStatus() {
		return messageStatus;
	} 

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}
	
	public String getResponseBody() {
		return message;
	}

	public void setResponseBody(String responseBody) {
		this.responseStatus = responseBody;
	}
	
	public ArrayList getTopicList() {
		return topics;
	}

	public void setTopicList(ArrayList topics) {
		this.topics = topics;
	}


}
