package ImqProtocol;

import com.google.gson.Gson;

public class Parser {
	
	public static String serlization(ResponseBody response) {
		Gson gson = new Gson();
		return gson.toJson(response);
	}

	public static RequestBody deSerlization(String request) {
		Gson gson = new Gson();
		return gson.fromJson(request, RequestBody.class);
	}
	
}
