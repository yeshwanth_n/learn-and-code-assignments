package Storage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import ImqProtocol.ResponseBody;
import utility.Constant;

public class DatabaseStorage {

    Connection connection;
    Statement statement;
    String clientTable = Constant.clientTable;

    private static DatabaseStorage single_instance = null;

    public static DatabaseStorage getInstance() {
        if (single_instance == null)
            single_instance = new DatabaseStorage();
        return single_instance;
    }

    public Connection getConnection() {
        try {
            connection = DriverManager.getConnection(Constant.url, Constant.user, Constant.password);
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
    
    public String pushMessage(String message, int portNumber, String topicID) {
        Statement statement;
        DatabaseStorage dataBase = DatabaseStorage.getInstance();

        try {
            Connection dataBaseConnection = dataBase.getConnection();
            statement = dataBaseConnection.createStatement();
            String insertClientData = "INSERT INTO  queue (topicId,pubID,message,createdTime,expiryTime) VALUES ('" + topicID + "','" + portNumber + "','" + message + "',sysDate(), ADDTIME(sysDate(), \"00:02:0.000000\"))";
            statement.executeUpdate(insertClientData);
            statement.close();
            return "success";
        } catch (SQLException e) {
            e.printStackTrace();
            return "failure";
        }
    }
    
    public ResponseBody pullMessage(String topicID) {
    	ResponseBody response = new ResponseBody();
        Statement statement;
        String messageStatus = "";
        DatabaseStorage dataBase = DatabaseStorage.getInstance();;
        Connection dataBaseConnection = dataBase.getConnection();
        try {
            statement = dataBaseConnection.createStatement();
            ResultSet results = statement.executeQuery("SELECT * FROM queue where topicId=" + topicID + " order by id limit 1");
            if (results.next()) {
                int id = results.getInt("id");
                String message = results.getString("message");
                int pubID = results.getInt("pubID");
                Timestamp createdtime = results.getTimestamp("createdTime");
                Timestamp date = results.getTimestamp("expiryTime");
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                if (date.before(timestamp)) {
                    try {
                        String insertClientData = "INSERT INTO  dead_letter_queue (topicId,pubID,message,createdTime,expiryTime) VALUES ('" + topicID + "','" + pubID + "','" + message + "','" + createdtime + "', '" + date + "')";
                        statement.executeUpdate("delete FROM queue where id=" + id);
                        statement.executeUpdate(insertClientData);
                        messageStatus = "Messege has been expired";
                        response.setMessage(message);
                        response.setMessageStatus(messageStatus);

                        statement.close();

                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                } else {
                    statement.executeUpdate("delete FROM queue where id=" + id);
                    messageStatus = "Message is processed and dequeued";
                    response.setMessage(message);
                    response.setMessageStatus(messageStatus);
                }
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return response;
    }
    
    public static void insertTopicData(String topicName) throws Exception {
    	 DatabaseStorage dataBase = DatabaseStorage.getInstance();
        Connection dataBaseConnection = dataBase.getConnection();
		PreparedStatement preparedStatement;
		boolean topicInserted = false;
		try 
		{
			preparedStatement = dataBaseConnection.prepareStatement("INSERT INTO topic (topicName)	SELECT * FROM (SELECT '" + topicName + "') AS tmp WHERE NOT EXISTS ( SELECT topicName FROM topic WHERE topicName='" + topicName + "')");
			preparedStatement.executeUpdate();				
		} 
		catch (SQLException exception) {
			throw new Exception(exception.getMessage());
		}  		
	}

	public ResultSet displayTopics() throws SQLException {
		   DatabaseStorage dataBase = DatabaseStorage.getInstance();;
		    Connection dataBaseConnection = dataBase.getConnection();
		try {
			 statement = dataBaseConnection.createStatement();
			 ResultSet results ;
	        results = statement.executeQuery("SELECT * FROM topic");
	        return results;  
		}catch(SQLException exception) {
			System.out.println("SQL Exception :"+ exception);
			return null;
		} 
	  }
}