package utility;


public class Constant {
	public static final String url = "jdbc:mysql://localhost:3306/img?autoReconnect=true&useSSL=false";
	public static final String user="root";
	public static final String password="Sapota123";
	public static final String clientTable = "client_table";
	public static  int port = 8880;
	
	public static final String publisher = "publisher";
	public static final String subscriber = "subscriber";
	public static final String createTopic = "createTopic";
	public static final String displayTopic = "displayTopic";
	public static final String encryptionSecret = "secret";
	public static final String QUIT = "QUIT";
	public static final String success_code_200 = "200";

}
