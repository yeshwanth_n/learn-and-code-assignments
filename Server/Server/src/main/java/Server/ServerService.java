package Server;

import java.io.DataOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import ImqProtocol.Parser;
import ImqProtocol.ResponseBody;
import Storage.DatabaseStorage;
import utility.Constant;
import utility.EncryptionUtils;

public class ServerService {

    static DatabaseStorage database = DatabaseStorage.getInstance();
    static String message;

    public static void pushMessageToDatabase(String message, DataOutputStream outStream, String topicId,
        int portNumber) throws IOException {
        String responseBody = Parser.serlization(generateSuccessResponse(message));
        outStream.writeUTF(responseBody);
        database.pushMessage(message, portNumber, topicId);
    }
    
    public static void pullMessageFromDatabase(String topicId, DataOutputStream outStream) throws IOException {
    	ResponseBody response = database.pullMessage(topicId);
        String jsonResponse = Parser.serlization(generateSuccessResponseSubscriber(response));
        outStream.writeUTF(jsonResponse);
    }
    
    public static String getTopicListFromDatabase() throws SQLException {
		ResultSet results= database.displayTopics();
		ArrayList<String> topics = new ArrayList<String>();
		
		while (results.next()) {
	        int topicId = results.getInt("id");
	        String topicName = results.getString("topicName");
	        String topicInfo=  String.valueOf(topicId)+" " + topicName ;
	        topics.add(topicInfo);
        }
     	String responseBody = Parser.serlization(generateSuccessTopicResponse(topics));
		return responseBody;
	}
    
    public static void createTopicInDatabase(DataOutputStream outStream, String message) throws Exception {
        String responseBody = Parser.serlization(generateSuccessCreateTopicResponse());
        outStream.writeUTF(responseBody);
        DatabaseStorage.insertTopicData(message);
	}

    public static ResponseBody generateSuccessResponse(String message) {
    	ResponseBody response = new ResponseBody();
        response.setMessageStatus("successfully delivered");
        response.setMessage(message);
        response.setResponseBody(Constant.success_code_200);
        return response;
    }

    public static ResponseBody generateSuccessResponseSubscriber(ResponseBody responseBody) {
    	ResponseBody response = new ResponseBody();
		String decryptMessage = EncryptionUtils.decrypt(responseBody.getMessage(),Constant.encryptionSecret);
		response.setMessage(decryptMessage);
        response.setMessageStatus(responseBody.getMessageStatus());
        response.setResponseBody(Constant.success_code_200);
        return response;
    }

    public static ResponseBody generateSuccessTopicResponse(ArrayList results) {
		ResponseBody response = new ResponseBody();
        response.setTopicList(results);
        response.setResponseBody(Constant.success_code_200);
        return response;
	}

	public static ResponseBody generateSuccessCreateTopicResponse() {
		ResponseBody response = new ResponseBody();
        response.setMessageStatus(":::: Topic Created sucessfully ::::");
        response.setResponseBody(Constant.success_code_200);
        return response;
	}
}