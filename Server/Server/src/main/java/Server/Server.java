package Server;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import utility.Constant;

public class Server {

    public static void main(String[] args) throws Exception {
        int port = Constant.port;
        int clientCounter = 0;
        Scanner scanner = new Scanner(System.in);
        try {
            ServerSocket server = createServerSocket(port);
            System.out.println("Server Started!!! Waiting for client connection!!!");
            establishClientConnection(clientCounter, server);
        } catch (Exception exception) {
            exception.printStackTrace();
            System.out.println("Connection Error !!");
        }
    }
    public static ServerSocket createServerSocket(int port) throws IOException {
        return new ServerSocket(port);
    }
    
    public static void establishClientConnection(int clientCounter, ServerSocket server) {
        while (true) {
            clientCounter++;
            Socket clientInfo;
            try {
                clientInfo = server.accept();
                System.out.println(" :::: Connection Established ::::");
                System.out.println(" :::: Client Number:" + clientCounter + " Started :::: ");
                ServerClientThread clientThread = new ServerClientThread(clientInfo, clientCounter);
                clientThread.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}