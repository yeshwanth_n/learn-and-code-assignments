package Server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.sql.Statement;
import ImqProtocol.Parser;
import ImqProtocol.RequestBody;
import utility.Constant;

class ServerClientThread extends Thread {
    protected Socket socketData;
    int clientNo;
    int storageChoiceNumber;
    Statement statement;

    ServerClientThread(Socket socket, int counter) {
        socketData = socket;
        clientNo = counter;
    }

    public void run() {
        boolean isConnected = socketData.isConnected();
        try {
            if (!isConnected) {
                throw new SeverConnectionException("Sorry!! Unable to connect!!!");
            } else {
                handleClientRequest(socketData);
            }
        } catch (Exception error) {
            error.printStackTrace();
        }
    }
    
    public static DataInputStream createDataInputStreamInstance(Socket socket) throws IOException {
      	 return new DataInputStream(socket.getInputStream());
    }
      	
    public static DataOutputStream createDataOutputStreamInstance(Socket socket) throws IOException {
         return new DataOutputStream(socket.getOutputStream());
    }

    private void handleClientRequest(Socket socketData) {
        try {
            DataInputStream dataInStream = createDataInputStreamInstance(socketData);
            DataOutputStream dataOutStream = createDataOutputStreamInstance(socketData);
            String clientMessage = "",request = "";
            RequestBody requestBody;
            String message;
            String topicId;
            int portNumber;
            String userType;
            String requestType;
            String topicName;
           
            while (!clientMessage.equals(Constant.QUIT)) {
                request = dataInStream.readUTF(dataInStream);    
                requestBody = Parser.deSerlization(request);
                userType = requestBody.getUserType();
                requestType=requestBody.getRequestType();
                message = requestBody.getMessage();
                portNumber = socketData.getPort();
                topicId = requestBody.getTopicId();
                topicName=requestBody.getTopicName();
                
                 if(requestType.equals(Constant.displayTopic)) { 
                     String topics= ServerService.getTopicListFromDatabase();
                     dataOutStream.writeUTF(topics);
                 }else if(requestType.equals(Constant.createTopic)) {
                	 ServerService.createTopicInDatabase(dataOutStream,topicName);
                     String topics= ServerService.getTopicListFromDatabase();
                     dataOutStream.writeUTF(topics);
                 }

                System.out.println(" :::: Request from Client :::: " + request);
                if (userType.equals(Constant.publisher) && requestType.equals(" ")) {
                    ServerService.pushMessageToDatabase(message, dataOutStream, topicId, portNumber);
                } else if (userType.equals(Constant.subscriber) && requestType.equals(" ")) {
                    ServerService.pullMessageFromDatabase(topicId, dataOutStream);
                }
            }
            dataOutStream.flush();
            dataInStream.close();
            dataOutStream.close();
        } catch (Exception error) {
            error.printStackTrace();
        } finally {
            System.out.println("Client " + clientNo + " exit!! ");
        }
    }
}

class SeverConnectionException extends Exception {
    public SeverConnectionException(String message) {
        super(message);
    }
}