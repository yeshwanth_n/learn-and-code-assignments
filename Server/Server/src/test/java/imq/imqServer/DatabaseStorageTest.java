package imq.imqServer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;

import Server.Server;
import Storage.DatabaseStorage;
import utility.Constant;

/**
 * Unit test for simple App.
 */
public class DatabaseStorageTest {    
	
    @Test
    public void testGetConnection() {
	    DatabaseStorage database =  DatabaseStorage.getInstance();
	    Connection connection = database.getConnection();
        assertNotNull( connection );
    }
    
    @Test
    public void testAddToDatabaseSuccess(){
    	String msg = "success";
    	String testMessage = "testMessage";
	    DatabaseStorage database =  DatabaseStorage.getInstance();
	    String response = database.pushMessage(testMessage, 007, "002");
	    assertEquals("ddd", msg,response );
    }
    
    @Test
    public void testAddToDatabaseFailure(){
    	String msg = "failure";
    	String testMessage = "testMessage";
	    DatabaseStorage database =  DatabaseStorage.getInstance();
	    String response = database.pushMessage(testMessage, 007, "aaa");
	    assertEquals("ddd", msg,response );
    }
    
    @Test
    public void testDisplayTopics() throws SQLException{
    	String testMessage = "testMessage";
	    DatabaseStorage database =  DatabaseStorage.getInstance();
	    ResultSet response = database.displayTopics();
	    assertNotNull(response);
    }
}
