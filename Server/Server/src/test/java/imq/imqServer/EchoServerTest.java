package imq.imqServer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.ServerSocket;
import java.sql.Connection;

import org.junit.Test;

import Server.Server;
import Storage.DatabaseStorage;
import utility.Constant;

/**
 * Unit test for simple App.
 */
public class EchoServerTest 
{
	@Test
    public void testServerSocketGetsCreated() throws IOException {
        assertNotNull(Server.createServerSocket(1234));
    }
    
    @Test
    public void testServerSocketWithSpecificPortGetsCreated() throws IOException {
        final int testPort = 9001;
        ServerSocket testServerSocket =
        		Server.createServerSocket(testPort);
        assertEquals(testServerSocket.getLocalPort(), testPort);
    }
    
}
