package imq.imqServer;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Test;

import ImqProtocol.ResponseBody;
import Server.Server;
import Server.ServerService;

public class TestServerService {
	
	@Test
    public void testGetTopicListFromDatabase() throws IOException, SQLException {
		String responseBody = ServerService.getTopicListFromDatabase();
        assertNotNull(responseBody);
    }
	
	@Test
    public void testGenerateSuccessResponse(){
		ResponseBody responseBody = ServerService.generateSuccessResponse("test_message");
        assertNotNull(responseBody);
    }
	
	@Test
    public void testGenerateSuccessResponseSubscriber(){
		ResponseBody testBody = new ResponseBody();
		testBody.setMessage("test_message");
		ResponseBody responseBody = ServerService.generateSuccessResponseSubscriber(testBody);
        assertNotNull(responseBody);
    }
	
	@Test
    public void testGenerateSuccessTopicResponse(){
		ArrayList<String> testArray = new ArrayList<String>();
		testArray.add("testMessage1");
		testArray.add("testMessage2");
		testArray.add("testMessage3");
		ResponseBody responseBody = ServerService.generateSuccessTopicResponse(testArray);
        assertNotNull(responseBody);
    } 
	
	@Test
    public void testGenerateSuccessCreateTopicResponse(){
		ResponseBody responseBody = ServerService.generateSuccessCreateTopicResponse();
        assertNotNull(responseBody);
    }
}
