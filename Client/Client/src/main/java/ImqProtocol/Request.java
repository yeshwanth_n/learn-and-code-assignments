package ImqProtocol;

public class Request {
	String requestType;
	String messageData;
	String userType;
	String topicOption;
	RequestBody requestBody;
	String topicList;

	
	//id, topicId, pubID, message, createdTime, expiryTime
	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public RequestBody getRequestBody() {
		return requestBody;
	}

	public void setRequestBody(RequestBody requestType) {
		this.requestBody = requestBody;
	}
	
	public String getTopicList() {
		return topicList;
	}

	public void setTopicList(String topicList) {
		this.topicList = topicList;
	}
	
	public String getRequestMessage() {
		return messageData;
	}

	public void setRequestMessage(String message) {
		this.messageData = message;
	}
	
	public String getUserType() {
		return userType;
	} 

	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	public String getTopicOption() {
		return topicOption;
	}

	public void setTopicOption(String topicOption) {
		this.topicOption = topicOption;
	}
}
