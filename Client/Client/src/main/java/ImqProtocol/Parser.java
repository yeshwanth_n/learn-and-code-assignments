package ImqProtocol;

import com.google.gson.Gson;

public class Parser {
	
	public static String serlize(Response response) {
		Gson gson = new Gson();
		return gson.toJson(response);
	}

	public static RequestBody deSerlize(String request) {
		Gson gson = new Gson();
		return gson.fromJson(request, RequestBody.class);
	}
	
}
