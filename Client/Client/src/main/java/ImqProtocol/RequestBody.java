package ImqProtocol;

import java.sql.Timestamp;
import java.util.ArrayList;

public class RequestBody {
	
	private String topicId;
	private Timestamp createdTime;
	private Timestamp expiryTime;
	private String message;
	private String userType;
	private String requestType;
	private String topicName;
	private ArrayList topics;

	
	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}
	
	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	
	public String getTopicId() {
		return topicId;
	}

	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}
 
	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}
	
	public Timestamp getExpiryTime() {
		return createdTime;
	}
	
	public void setExpiryTime(Timestamp expiryTime) {
		this.expiryTime = expiryTime;
	}
	
	public Timestamp getCreatedTime() {
		return expiryTime;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public ArrayList getTopicList() {
		return topics;
	}

	public void setTopicList(ArrayList topics) {
		this.topics = topics;
	}

	
}
