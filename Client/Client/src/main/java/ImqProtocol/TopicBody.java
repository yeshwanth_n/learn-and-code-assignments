package ImqProtocol;

public class TopicBody {
	
	    int id;
		String topicName;

		public int getID() {
			return id;
		}

		public void setID(int id) {
			this.id = id;
		}

		public String getTopicName() {
			return topicName;
		}

		public void setTopicName(String topicName) {
			this.topicName = topicName;
		}

}
