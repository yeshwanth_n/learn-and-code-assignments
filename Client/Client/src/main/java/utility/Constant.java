package utility;


public class Constant {
	public static  int port = 8880;
	public static final String address="127.0.0.1";
	
	public static final String publisher = "publisher";
	public static final String subscriber = "subscriber";
	public static final String createTopic = "createTopic";
	public static final String displayTopic = "displayTopic";
	public static final String encryptionSecret = "secret";
	public static final String QUIT = "QUIT";
	public static final String CHAR_y = "y";

}
