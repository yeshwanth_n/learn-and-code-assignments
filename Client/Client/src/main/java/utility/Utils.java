package utility;

import com.google.gson.Gson;

import ImqProtocol.RequestBody;

public class Utils {
	
	public static String convertToJson(RequestBody request) {
		 Gson gson = new Gson();
		 String message = gson.toJson(request);
		return message; 
	}
	
	public static RequestBody generateRequest( String message, String topicId,String userType ) {
		RequestBody requestBody= new RequestBody();
		requestBody.setMessage(message);
		requestBody.setTopicId(topicId);
		requestBody.setUserType(userType);
		requestBody.setRequestType(" ");
		return requestBody;
    }
}
