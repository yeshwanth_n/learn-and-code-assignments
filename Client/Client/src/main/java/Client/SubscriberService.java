package Client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;

import ImqProtocol.RequestBody;
import utility.Constant;
import utility.Utils;

public class SubscriberService extends Service{
	
	public static void displayTopicToSubscriber(DataInputStream inStream, DataOutputStream outStream, BufferedReader read) {
		try {
			int topicSize = displayTopic(Constant.subscriber, inStream,  outStream,  read);
			pullMessage(getTopicId(topicSize),Constant.subscriber, inStream,  outStream,  read);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e ) {
		    System.out.println("Something went wrong! Please try again");
		}
	}
	
	private static void pullMessage(String topicID,String userType, DataInputStream inStream, DataOutputStream outStream, BufferedReader read) throws UnknownHostException, IOException {
		  String clientMessage = "";
			  while (!clientMessage.equals(Constant.QUIT)) {
				    System.out.println("Enter Data y to pull the message (Enter QUIT to end):\"");
				    clientMessage = read.readLine();
					if(clientMessage.equals(Constant.CHAR_y)) {
					    RequestBody request = Utils.generateRequest("Pull the message !!!!",topicID,userType);
					    String message= Utils.convertToJson(request);
					    outStream.writeUTF(message);
					    outStream.flush();
					    String responseObject = inStream.readUTF();
					    System.out.println("Response to Client " + responseObject);
					}
			  }
	}

}
