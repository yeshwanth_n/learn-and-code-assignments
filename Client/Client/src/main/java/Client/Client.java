package Client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Scanner;
import utility.Constant;

public class Client  {
	
	public static void main(String[] args) throws Exception {
		Socket socket = new Socket(Constant.address, Constant.port);
		BufferedReader read = Service.createBufferedReaderInstance(socket);
		DataInputStream inStream = Service.createDataInputStreamInstance(socket);
		DataOutputStream outStream = Service.createDataOutputStreamInstance(socket);

		   Scanner scanner = new Scanner(System.in);
		   System.out.println("Please choose the role");
           System.out.println("Choose 1 for Publisher");
           System.out.println("Choose 2 for Subscriber");
           int roleNumber = scanner.nextInt();
           switch (roleNumber) {
               case 1:
                   System.out.println("The role choosen : Publisher ");
                   PublisherService.displayOptions(inStream,outStream, read);
                   break;
               case 2:
                   System.out.print("The role choosen :  Subscriber ");
                   SubscriberService.displayTopicToSubscriber(inStream,outStream, read);
                   break;
               case 3:
                   System.exit(0);
                   break;
               default:
                   System.out.println("Invalid role");
           }
		  	
	}	
}
