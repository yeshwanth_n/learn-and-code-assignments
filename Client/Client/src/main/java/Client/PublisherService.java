package Client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Scanner;


import ImqProtocol.RequestBody;
import utility.Constant;
import utility.EncryptionUtils;
import utility.Utils;

public class PublisherService extends Service{
	
	static void displayOptions(DataInputStream inStream, DataOutputStream outStream, BufferedReader read) throws UnknownHostException, IOException {
		System.out.println("Please choose your option");
        System.out.println("Choose 1 for Displaying the topic");
		System.out.println("Choose 2 for Creating the topic");
        handleOptions(takeUserInput(), inStream,  outStream,  read);
	} 
	
	private static int takeUserInput() {
        return scanner.nextInt();
    }
	
	private static void handleOptions(int optionNumber, DataInputStream inStream, DataOutputStream outStream, BufferedReader read) throws UnknownHostException, IOException {
		 switch (optionNumber) {
         case 1:
        	 int topicSize = displayTopic(Constant.publisher, inStream,  outStream,  read);
        	 publishMessage(getTopicId(topicSize),Constant.publisher, inStream,  outStream,  read);
             break;
         case 2:
        	 createtopic(Constant.publisher, inStream,  outStream,  read);
             break;
         case 3:
             System.exit(0);
             break;
         default:
             System.out.println("Invalid Option");
     }  
	}
	
	protected static void createtopic(String userType, DataInputStream inStream, DataOutputStream outStream, BufferedReader read) throws UnknownHostException, IOException {
		  String clientMessage = "";
		  while (!clientMessage.equals(Constant.QUIT)) {
			    System.out.println("Enter the topic name");
			    clientMessage = read.readLine();
			    RequestBody request = generateCreateTopicRequest(userType,Constant.createTopic,clientMessage);
			    String message= Utils.convertToJson(request);
			    outStream.writeUTF(message);
			    outStream.flush();
			    String responseObject = inStream.readUTF();
			    System.out.println("Response to Client " + responseObject);
			    publishMessage(getTopicId(),Constant.publisher, inStream,  outStream,  read);
		  }
	}
	
	@SuppressWarnings("finally")
	public static String getTopicId(int topicSize) {
		int topicsCount = topicSize;
		Scanner scanner = new Scanner(System.in); 
		System.out.print("Enter the Topic ID\n");
		  try {
				String topicId = scanner.nextLine();	

	        	if(topicsCount < Integer.parseInt(topicId)) {
	        	    throw new InvalidTopicIDException("Invalid Topic ID!!!");
	            }else {
	                return topicId;
	            }  
	        }catch(InvalidTopicIDException e) {
	        	e.printStackTrace();
	        }finally {
	        	System.out.println("Please Enter Valid Topic ID");
				return scanner.nextLine();	
	        }
	}
	
	private static RequestBody generateCreateTopicRequest(String userType, String requestType,String topicName) {
		RequestBody requestBody= new RequestBody();
		requestBody.setUserType(userType);
		requestBody.setRequestType(requestType);
		requestBody.setTopicName(topicName);
		return requestBody;
	}
	
	public static void publishMessage(String topicID,String userType, DataInputStream inStream, DataOutputStream outStream, BufferedReader read) throws UnknownHostException, IOException {
		  String clientMessage = "";
			  while (!clientMessage.equals(Constant.QUIT)) {
				    System.out.println("Enter Data to echo Server (Enter QUIT to end):\"");
				    clientMessage = read.readLine();
				    String encryptedMessage = EncryptionUtils.encrypt(clientMessage, Constant.encryptionSecret);
				    RequestBody request = Utils.generateRequest(encryptedMessage,topicID,userType);
				    String message= Utils.convertToJson(request);
				    outStream.writeUTF(message);
				    outStream.flush();
				    String responseObject = inStream.readUTF();
				    System.out.println("Response to Client " + responseObject);
		     } 
	}
}

class InvalidTopicIDException extends Exception {
    public InvalidTopicIDException(String message) {
        super(message);
    }
}
