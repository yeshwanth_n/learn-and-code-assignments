package Client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Scanner;
import com.google.gson.Gson;

import ImqProtocol.Parser;
import ImqProtocol.Request;
import ImqProtocol.RequestBody;
import ImqProtocol.Response;
import utility.Constant;

public class Service {
	
	 static String address = Constant.address;
	 static int port = Constant.port;
	 static Scanner scanner = new Scanner(System.in);
	 static Request response;
	
	 public static DataInputStream createDataInputStreamInstance(Socket socket) throws IOException {
		 return new DataInputStream(socket.getInputStream());
		}
		
		public static DataOutputStream createDataOutputStreamInstance(Socket socket) throws IOException {
			 return new DataOutputStream(socket.getOutputStream());
		}
		
		public static BufferedReader createBufferedReaderInstance(Socket socket) throws IOException {
			 return new BufferedReader(new InputStreamReader(System. in ));
		}

	protected static int displayTopic(String userType, DataInputStream inStream, DataOutputStream outStream, BufferedReader read) throws UnknownHostException, IOException {
				    System.out.println("Display topic\"");
				    RequestBody request = generateTopicRequest(userType,Constant.displayTopic);
				    String message= convertToJson(request);
				    System.out.println(message);
				    outStream.writeUTF(message);
				    outStream.flush();
				    String responseObject = inStream.readUTF();
				    ArrayList topics = Parser.deSerlize(responseObject).getTopicList();
				    for (int i = 0; i < topics.size(); i++) {
			            System.out.println(topics.get(i));
			        }
				    
				    return topics.size();
	} 
	
	public static String getTopicId() {
		Scanner scanner = new Scanner(System.in); 
		System.out.print("Enter the Topic ID\n");
		return scanner.nextLine();		
	}
	
	@SuppressWarnings("finally")
	public static String getTopicId(int topicSize) {
		int topicsCount = topicSize;
		Scanner scanner = new Scanner(System.in); 
		System.out.print("Enter the Topic ID\n");
		  try {
				String topicId = scanner.nextLine();	
	        	if(topicsCount < Integer.parseInt(topicId)) {
	        	    throw new InvalidTopicIDException("Invalid Topic ID!!!");
	            }else {
	                return topicId;
	            }  
	        }catch(InvalidTopicIDException e) {
	        	e.printStackTrace();
	        }finally {
	        	System.out.println("Please Enter Valid Topic ID");
				return scanner.nextLine();	
	        }
	}
	
	
	private static RequestBody generateTopicRequest(String userType,String requestType ) {
		RequestBody requestBody= new RequestBody();
		requestBody.setUserType(userType);
		requestBody.setRequestType(requestType);
		return requestBody;
    }
	 	
	 public static String convertToJson(RequestBody request) {
		 Gson gson = new Gson();
		 String message = gson.toJson(request);
		return message;	 
	 }
	 
	public static Request getRequest(String request) {
		Gson gson = new Gson();
		return gson.fromJson(request, Request.class);
	}
		
	public static Response getResponse(String request) {
		Gson gson = new Gson();
		return gson.fromJson(request, Response.class);
	}
}
