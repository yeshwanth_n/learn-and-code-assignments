package imq.client;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import org.junit.Test;
import org.mockito.Mock;

import Client.Service;
import ImqProtocol.Request;
import ImqProtocol.RequestBody;

/**
 * Unit test for simple App.
 */
public class TestService 
{
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    
    @Mock
    Socket clientSocket;
    
    @Mock
    InputStream inputStream;
    
    @Mock
    OutputStream outputStream;
    
    @Test
    public void testSocketInputGetsCreated() throws IOException {
        when(clientSocket.getInputStream()).thenReturn(inputStream);
        assertNotNull(Service.createDataInputStreamInstance(clientSocket));
    }
    
    @Test
    public void testGetRequest() {
    	Request request = Service.getRequest("msg");
        System.out.println("Choose 2 for Subscriber role===="+ request);

        assertNotNull(request);
    }
    
    @Test
    public void testConvertToJson() {
    	RequestBody requestBody= new RequestBody();
		requestBody.setUserType("test_user_type");
		requestBody.setRequestType("test_request_type");
    	String request = Service.convertToJson(requestBody);
        assertNotNull(request);
    }
   
}
